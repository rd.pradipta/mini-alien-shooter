# Mini Alen Shooter

Mini Alien Shooter is a simple 2.5D game game built by using Unity3D game engine. This game was intented for RISTEK Fasilkom UI 2019 Game Development Special Interest Group Open Recruitment task.

How To Play:
1. Go to folder "Ready to Play"
2. Double click on file "Mini Alien Shooter.exe" file
3. Enjoy The Game

Rules:
1. This game only has 1 level
2. Each level consist of Player as Plane and Enemies
3. Player mush shoot all the enemies before enemies collides with plane or come out of game area
4. Each enemy shooted worth 10 points
5. Player wins if successfully shooted all the enemies

Created with love by: Pradipta Gitaya